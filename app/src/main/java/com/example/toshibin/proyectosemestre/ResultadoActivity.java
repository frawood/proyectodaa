package com.example.toshibin.proyectosemestre;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public class ResultadoActivity extends AppCompatActivity {

    private RecyclerView recView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layMan;
    private ArrayList<Trabajador> datos = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);

        obtenerListado();

        recView = (RecyclerView) findViewById(R.id.listado);
        recView.setHasFixedSize(true);
        layMan = new LinearLayoutManager(this);
        recView.setLayoutManager(layMan);
        adapter = new TrabajadorAdapter(this, datos, getIntent().getIntExtra("tipoBusqueda",1));
        recView.setAdapter(adapter);
        //recView.addItemDecoration(new DividerItemDecoration(this,));
        recView.addOnItemTouchListener(new RecViewClickListener(ResultadoActivity.this, new RecViewClickListener.OnItemClickListener(){
        @Override
        public void onItemClick(View view, int position) {
            Intent i= new Intent(ResultadoActivity.this,ItemPerfilActivity.class);
            i.putExtra("master", datos.get(position).toString());
            i.putExtra("tipoBusqueda", getIntent().getIntExtra("tipoBusqueda",1));
            i.putExtra("rubro",getIntent().getStringExtra("rubro"));
            startActivity(i);
        }
    }) {

    });
    }

    private void obtenerListado() {
        Gson gson = new Gson();
        String jsonBD = WSDatos.getInstance().listaTrabajadores();
        datos = gson.fromJson(jsonBD, new TypeToken<List<Trabajador>>(){}.getType());
    }

    /*private void obtenerListado() {
        new ObtieneDatos(this).execute();
    }

    private class ObtieneDatos extends AsyncTask<Void, Void, ArrayList<Trabajador>> {

        private final Context context;
        private ProgressDialog mProgress;

        public ObtieneDatos(Context context){
            this.context = context;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgress = new ProgressDialog(context);
            mProgress.setIndeterminate(true);
            mProgress.setMessage("Obteniendo Información");
            mProgress.setCancelable(false);
            mProgress.show();

        }

        @Override
        protected ArrayList<Trabajador> doInBackground(Void... params) {

            try{
                Gson gson = new Gson();
                String jsonBD = WSDatos.getInstance().listaTrabajadores();
                datos = gson.fromJson(jsonBD, new TypeToken<List<Trabajador>>(){}.getType());
                cargarDatos(datos);

            }catch (Exception e){

            }
            return datos;
        }

        @Override
        protected void onPostExecute(ArrayList<Trabajador> datosT) {
            super.onPostExecute(datosT);
            mProgress.hide();
            //cargarDatos();
        }
    }

    private void cargarDatos(ArrayList<Trabajador> maestros){
        datos = maestros;
    }*/
}
