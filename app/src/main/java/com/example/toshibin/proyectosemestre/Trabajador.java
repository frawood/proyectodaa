package com.example.toshibin.proyectosemestre;


public class Trabajador extends Persona {

    private int rubro;
    private String descripcion;
    private int ubicacion;
    private int calificacion = 0;

    public Trabajador() {
    }

    public Trabajador(String nombre, String direccion, String comuna, String telefono, int rubro, String descripcion, int ubicacion) {
        super(nombre, direccion, comuna, telefono);
        this.setRubro(rubro);
        this.setDescripcion(descripcion);
        this.setUbicacion(ubicacion);
    }

    public int getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(int calificacion) {
        this.calificacion = calificacion;
    }

    public int getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(int ubicacion) {
        this.ubicacion = ubicacion;
    }

    public void setRubro(int rubro) {
        this.rubro = rubro;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getRubro() {
        return rubro;
    }

    public String getDescripcion() {
        return descripcion;
    }

    @Override
    public String toString() {
        return super.toString()+"rubro: '" + rubro +
                "', descripcion: '" + descripcion +
                "', ubicacion: '" + ubicacion +
                "', calificacion: '" + calificacion+
                "'}";
    }
}
