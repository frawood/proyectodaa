package com.example.toshibin.proyectosemestre;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

public class BusquedaActivity extends AppCompatActivity {

    private Button btnBuscar;
    private RadioButton rbtnEmergencia;
    private RadioButton rbtnParticular;
    private Spinner rubro;
    private TextView txtNombreUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_busqueda);
        

        rbtnEmergencia =(RadioButton)findViewById(R.id.rbtnEmergencia);
        rbtnParticular = (RadioButton)findViewById(R.id.rbtnParticular);
        btnBuscar=(Button)findViewById(R.id.btnBuscar);
        rubro=(Spinner)findViewById(R.id.rubro);
        txtNombreUsuario = (TextView) findViewById(R.id.txtNombreUsuario);

        Gson gson = new Gson();
        String jsonUsuario= Sesion.getInstancia().obtenerUsuario(this);
        Persona persona = gson.fromJson(jsonUsuario, Persona.class);
        txtNombreUsuario.setText("Bienvenido "+persona.getNombre().toString());

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buscarMaestro();
            }
        });
    }

    private void buscarMaestro() {
        if(valida()) {
            int tipo = 0;
            if (rbtnEmergencia.isChecked())
                tipo = 1;
            else
                tipo = 2;
            buscarMaestro(tipo, rubro.getSelectedItemPosition());
        }
        else
            Toast.makeText(BusquedaActivity.this, "Seleccionar campos", Toast.LENGTH_SHORT).show();


    }


    private void buscarMaestro(int tipoBusqueda, int rubro) {
        Intent i = new Intent(this, ResultadoActivity.class);
        i.putExtra("tipoBusqueda", tipoBusqueda);
        i.putExtra("rubro",this.rubro.getSelectedItem().toString());
        startActivity(i);
    }

    private boolean valida() {
        boolean validacion;
        if(rbtnEmergencia.isChecked()||rbtnParticular.isChecked()&&rubro.getSelectedItemPosition()!=0){
            validacion=true;
        }
        else validacion=false;
        return validacion;
    }
}
