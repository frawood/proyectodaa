package com.example.toshibin.proyectosemestre;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.gson.Gson;

public class ItemPerfilActivity extends AppCompatActivity {

    private TextView txtNombre;
    private TextView txtDireccion;
    private TextView txtComuna;
    private TextView txtTelefono;
    private TextView txtRubro;
    private TextView txtDescripcion;
    private TextView txtUbicacion;
    private RatingBar calificacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_perfil);

        Gson gson = new Gson();
        Trabajador t = gson.fromJson(getIntent().getStringExtra("master"), Trabajador.class);

        txtNombre = (TextView) findViewById(R.id.masterNombre);
        txtComuna = (TextView) findViewById(R.id.masterComuna);
        txtDireccion=(TextView)findViewById(R.id.masterDirec);
        txtTelefono=(TextView)findViewById(R.id.masterTelefono);
        txtRubro = (TextView)findViewById(R.id.masterRubro);
        txtDescripcion = (TextView)findViewById(R.id.masterDescripcion);
        txtUbicacion = (TextView)findViewById(R.id.masterUbicacion);
        calificacion = (RatingBar) findViewById(R.id.masterCalificacion);

        txtNombre.setText(t.getNombre().toString());
        txtComuna.setText("Comuna: "+t.getComuna().toString());
        txtDireccion.setText("Dirección: "+t.getDireccion().toString());
        txtTelefono.setText("Telefono de contacto: "+t.getTelefono().toString());
        txtRubro.setText("Especialidad: "+getIntent().getStringExtra("rubro").toString());
        txtDescripcion.setText("Descripción Personal: "+t.getDescripcion().toString());
        if(getIntent().getIntExtra("tipoBusqueda",1) == 1)
            txtUbicacion.setText("Ubicación: a "+t.getUbicacion()+" metros de distancia");
        calificacion.setRating((int)t.getCalificacion());

    }
}
