package com.example.toshibin.proyectosemestre;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;


import java.util.ArrayList;


/**
 * Created by toshibin on 01-06-2016.
 */
public class TrabajadorAdapter extends RecyclerView.Adapter<TrabajadorAdapter.ViewHolder>{

    private final Context context;
    private final ArrayList<Trabajador> datos;
    private final int tipoBusqueda;

    public TrabajadorAdapter(Context context, ArrayList<Trabajador> datos, int tipoBusqueda){
        this.context = context;
        this.datos = datos;
        this.tipoBusqueda=tipoBusqueda;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.trabajador_item, viewGroup, false);
        ViewHolder vh=new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int i) {
        final Trabajador t=datos.get(i);
        holder.txtNombre.setText(t.getNombre().toString());
        if(tipoBusqueda == 1)
            holder.txtUbicacion.setText("Ubicación: a "+t.getUbicacion()+" metros");
        else
            holder.txtUbicacion.setText(t.getComuna().toString());
        holder.rtbar.setRating((int)t.getCalificacion());
    }

    @Override
    public int getItemCount() {
        return datos.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtNombre;
        public TextView txtUbicacion;
        public RatingBar rtbar;

        public ViewHolder(View v) {
            super(v);
            txtNombre = (TextView)v.findViewById(R.id.txtNombre);
            txtUbicacion = (TextView)v.findViewById(R.id.txtUbicacion);
            rtbar = (RatingBar) v.findViewById(R.id.ratingBar);
        }
    }
}
